<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Indice album fotos Joaquin</title>
        <?php
        require 'partials/cabecera.part.php';
        ?>
        <link href="css/blog.css" rel="stylesheet">
    </head>
    <body>
        <header> 
            <?php
            require 'partials/nav.part.php';
            ?>
        </header>
        <div class="posts">
            <div id="contenedor" class="container">

            </div>
        </div>
        <br>
        <hr>
        <footer class="footer">
            <?php
            require 'partials/footer.part.php';
            ?>
        </footer>
        <?php
        require 'js/cargaScripts.php';
        ?>
        <script src="js/cargaPosts.js" type="text/javascript"></script>
    </body>
</html>