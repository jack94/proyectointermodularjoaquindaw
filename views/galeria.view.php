<!doctype html>
<html lang="en">
    <head>
        <?php
        require 'partials/cabecera.part.php';
        ?>
        <link href="css/galeria.css" rel="stylesheet">
        <!--        <link rel="stylesheet" href="css/jquery.fancybox.min.css" rel="stylesheet">-->
    </head>
    <body>
        <header>
            <?php
            require 'partials/nav.part.php';
            ?>
        </header>
        <p class="h2 text-center mt-4 font-italic"> Galeria imagenes</p>

        <div id="contenedor" class="container-fluid mt-2 p-5">


        </div>

        <footer class="footer">
            <?php
            require 'partials/footer.part.php';
            ?>
        </footer>
        <?php
        require 'js/cargaScripts.php';
        ?>
        
        <script src="js/galeria.js" type="text/javascript"></script>



    </body>
</html>