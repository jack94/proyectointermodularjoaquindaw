<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Indice album fotos Joaquin</title>
        <?php
        require 'partials/cabecera.part.php';
        ?>
        <link href="css/index.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <?php
            require 'partials/nav.part.php';
            ?>
        </header>
        <section id="izquierda" class="row laterales">
        
        </section>
        <section id="centro" class="row rounded mt-3 pt-3 pl-5 ">
            <p class="h1 font-italic text-center offset-2 mx-auto img-fluid">PROYECTO INTERMODULAR JOAQUIN</p>
            <div class="contenedorSlider col-12 mx-auto">
                <div class="slide rounded">
                    <div class="indiceFotos text-dark rounded">1 / 3</div>
                    <img src="imgs/galeria1.jpg" class="rounded" style="width:100%">
                </div>
                <div class="slide rounded">
                    <div class="indiceFotos text-dark rounded">2 / 3</div>
                    <img src="imgs/galeria5.jpg" style="width:100%">
                </div>
                <div class="slide rounded">
                    <div class="indiceFotos text-dark rounded">3 / 3</div>
                    <img src="imgs/galeria7.jpg" style="width:100%">
                </div>
                <a id="anterior" class="anterior rounded" >&#10094;</a>
                <a id="siguiente" class="siguiente rounded" >&#10095;</a>   
            </div>
        </section>
        <section id="derecha" class="row laterales">
        
        </section>
        <div id="banerCookies" class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>¡Alerta sobre las cookies!</strong>-Si sigues navegando en nuestra web significa que aceptas nuestra politica de privacidad y cookies.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!--     <br>

<div style="text-align:center">
<span class="boton" onclick="indice(1)"></span> 
<span class="boton" onclick="indice(2)"></span> 
<span class="boton" onclick="indice(3)"></span> 
</div>
-->
        <footer class="footer">
            <?php
            require 'partials/footer.part.php';
            ?>
        </footer>
        <?php
        require 'js/cargaScripts.php';
        ?>
        <script src="js/index.js" type="text/javascript"></script>
    </body>
</html>