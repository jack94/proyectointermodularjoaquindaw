<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Indice album fotos Joaquin</title>
        <?php
        require 'partials/cabecera.part.php';
        ?>
        <link href="css/form.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <?php
            require 'partials/nav.part.php';
            ?>
        </header>
        <div id="contenedor" class="container-fluid">
            <form>
                <div class="row text-center bg-secondary">
                    <p class="h1 text-secondary col-md-3 mt-4 offset-lg-4 offset-md-3 offset-sm-2 offset-3 text-light">Login</p>
                    <fieldset class="offset-lg-3 col-lg-8 offset-md-2 col-md-10 col-sm-10 offset-sm-1 col-10 offset-1">
                        <div class="form-group mt-4">
                            <div class="col-md-4 offset-md-2">
                                <input id="email" name="email" type="email" placeholder="Email" class="form-control" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
                            </div>
                        </div>
                        <div class="form-group mt-4">
                            <div class="col-md-4 offset-md-2">
                                <input id="pass" name="pass" type="text" placeholder="Introduce tu contraseña" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <div id="divBoton2" class="col-4 p-4">
                        <button id="enviaForm" class="btn-light text-secondary btn-lg ml-5">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
        <footer class="footer">
            <?php
            require 'partials/footer.part.php';
            ?>
        </footer>
        <?php
        require 'js/cargaScripts.php';
        ?>
        <script src="js/login.js" type="text/javascript"></script>
    </body>
</html>