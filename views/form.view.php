<!DOCTYPE HTML>
<html>
    <head>
        <?php
        require 'partials/cabecera.part.php';
        ?>
        <link href="css/form.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <?php
            require 'partials/nav.part.php';
            ?>
        </header>
        <div>
            <form class="container-fluid" method="post">
                <div class="row">
                    <p class="h1 text-secondary mt-4 offset-lg-4 offset-md-3 offset-sm-2 offset-1">Formulario de registro</p>
                    <fieldset class="offset-lg-3 col-lg-8 offset-md-2 col-md-10 col-sm-10 offset-sm-1 col-10 offset-1">
                        <div class="form-group mt-3">
                            <div class="col-md-8">
                                <input id="nombre" name="nombre" type="text" placeholder="Nombre" class="form-control" minlength="4" maxlength="25" required="required">
                            </div>
                        </div>
                        <div class="form-group mt-4">

                            <div class="col-md-8">
                                <input id="primerApe" name="primerApe" type="text" placeholder="Primer apellido" class="form-control" minlength="4" maxlength="20" required>
                            </div>
                        </div>
                        <div class="form-group mt-4">

                            <div class="col-md-8">
                                <input id="segunApe" name="segunApe" type="text" placeholder="Segundo apellido" class="form-control"
                                       minlength="4" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group mt-4">

                            <div class="col-md-8">
                                <input id="email" name="email" type="email" placeholder="Email" class="form-control" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
                            </div>
                        </div>
                        <div class="form-group mt-4">

                            <div class="col-md-8">
                                <input id="pass" name="pass" type="text" placeholder="Introduce una contraseña (minimo 8 caracteres, 1 mayuscula 1 numero, sin caracteres especiales)" class="form-control" minlength="8">
                            </div>
                        </div>
                        <div class="form-group mt-4">

                            <div class="col-md-8">
                                <input id="repitePass" name="repitePass" type="text" placeholder="Repite la contraseña" class="form-control" minlength="8">
                            </div>
                        </div>
                        <div class="form-group mt-4">

                            <div class="col-md-8">
                                <textarea class="form-control" id="comentario" name="comentario" placeholder="Este es tu espacio en caso de que quieras hacernos llegar alguna cuestion surgida durante el registro" rows="6" minlength="10" maxlength="120"></textarea>
                            </div>

                        </div>
                    </fieldset>
                    <div id="divBoton" class="col-4 p-4">
                        <button id="enviaForm" class="btn-secondary btn-lg ml-5">Enviar</button>
                    </div>
                </div>
            </form>
            <!--
<div id="divBoton" class="col-4 p-4">
<button id="enviaForm" class="btn-secondary btn-lg ml-5">Enviar</button>
</div>
-->
        </div>
        <footer class="footer">
            <?php
            require 'partials/footer.part.php';
            ?>
        </footer>
        <?php
        require 'js/cargaScripts.php';
        ?>
        <script src="js/validaForm.js" type=text/javascript></script>
    </body>
</html>