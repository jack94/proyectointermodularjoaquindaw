<div class="collapse bg-secondary" id="footerInfo">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
                <h4 class="text-white">Redes sociales</h4>
                <p class="text-white bg-white rounded text-secondary font-italic">Twiiter Facebook Instagram TikTok</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
                <h4 class="text-white">Contacto</h4>
                <ul class="list-unstyled">
                    <li><a href="#" class="text-white">joaquinclinares@gmail.com</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-dark bg-secondary box-shadow">
    <div class="container d-flex justify-content-between">
        <p class="float-left">
            <a href="#" class="text-light">Volver al principio</a>
        </p>
        <a href="#" class="navbar-brand d-flex align-items-center">
        </a>
        <button class="navbar-toggler float-left bg-secondary" type="button" data-toggle="collapse" data-target="#footerInfo" aria-controls="navbarFooter" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</div>