

<nav class="navbar navbar-dark navbar-expand-md bg-secondary">
    
    <a class="navbar-brand text-light pl-4" href="index.php"><img src="icons/camara.png">
        <strong class="pl-3">Album</strong></a>
    <button class="navbar-toggler bg-secondary" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse pl-5" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active text-light" href="index.php">Home<span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link text-light" href="blog.php">Blog</a>
            <a class="nav-item nav-link text-light" href="galeria.php">Galeria</a>
            <a class="nav-item nav-link text-light" href="form.php">Contacto</a>
            <a class="nav-item nav-link text-light" href="login.php">Login</a>
        </div>
    </div>
</nav>