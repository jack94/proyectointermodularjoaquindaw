$(document).ready(function(){
    cargaPosts();

});
numeroPar=2;
function cargaPosts(){
    var contenedor=$('#contenedor');
    $.get("jsons/posts.json", function(data, status){

        if(status == "success"){
            for(let i=0;i<data['posts'].length;i++){
                var row=$('<div class="row align-items-center m-5 border p-3"></div>');

                var divImg=$('<div class="col-12 col-md-6"></div>');

                var img=$('<img class="img-fluid" src="'+data['posts'][i].src+'" />');

                var divContenido=$('<div class="col-12 col-md-6"></div>');

                var divTexto=$('<div class="text-center"></div>');

                var titulo=$('<p class="font-italic h3 pb-2">'+data['posts'][i].titulo+'</p>"');
                var fecha=$('<p class="float-left font-italic">'+data['posts'][i].fecha+'</p>');

                var br=$('<br>');
                var hr=$('<hr>');

                var parrafo=$('<div class="float-left">'+data['posts'][i].contenido+'</div>');
                var autor=$('<div class="text-center float-right font-italic">Escrito por: '+data['posts'][i].autor+'</div>');

                var br=$('<br>');
                if (numeroPar % 2 == 0){
                    divImg.append(img);
                    row.append(divImg);

                    divTexto.append(titulo,fecha,parrafo,autor);
                    divContenido.append(divTexto);
                    row.append(divContenido);
                }else{
                    divTexto.append(titulo,fecha,parrafo,autor);
                    divContenido.append(divTexto);
                    row.append(divContenido);

                    divImg.append(img);
                    row.append(divImg);
                }
                contenedor.append(row,hr);
                numeroPar++;
            }
        }else{
            alert("Error al cargar fichero externo via ajax");
        }
    });
}
