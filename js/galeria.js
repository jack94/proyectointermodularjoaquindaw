$(document).ready(function(){
    
    $('.footer').append(fancyCss,fancyJs);
    cargaImagenes(); 
    
    var fancyCss=$('<link rel="stylesheet" href="css/jquery.fancybox.min.css" rel="stylesheet" media="screen">');
    var fancyJs=$('<script src="js/jquery.fancybox.min.js" type="text/javascript"></script>');

    $('.footer').append(fancyCss,fancyJs);
    $(".fancybox").fancybox({
        openEffect: "elastic",
        closeEffect: "elastic"
    }); 
    $(".zoom").hover(function(){

        $(this).addClass('transition');
    }, function(){

        $(this).removeClass('transition');
    });
});

function cargaImagenes(){
    var contenedor=$('#contenedor');
    $.get("jsons/galeria.json", function(data, status){

        if(status == "success"){  
            var row=$('<div class="row"></div>');
            for(let i=0;i<data['imagenes'].length;i++){

                var divImg=$('<div class="col-lg-3 col-md-4 offset-sm-0 col-sm-6 col-12 offset-1 mb-5"></div>');

                var fancyBox=$('<a href="'+data['imagenes'][i].src+'" class="fancybox" rel="ligthbox"></a>"');

                var img=$('<img src="'+data['imagenes'][i].src+'" class="zoom img-fluid">');

                fancyBox.append(img);
                divImg.append(fancyBox);
                row.append(divImg);

            }
            contenedor.append(row);
        }else{
            alert("Error al cargar fichero externo via ajax");
        }
    });
}