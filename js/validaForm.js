$(document).ready(function() {
    $("#enviaForm").click(function() {
        validaFormulario();
    });
});
function validaFormulario(){
    var nomb,ape1,ape2,correo,contr1,contr2,coment=false;

    var expreReguDni = /^\d{8}[a-zA-Z]$/;
    var expreReguEmail = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //    var expreReguPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;

    var expreReguPass =/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;

    var nombre = $("#nombre").val();
    var primerApe = $("#primerApe").val();
    var segunApe = $("#segunApe").val();

    var email = $("#email").val();
    var pass= $("#pass").val();
    var repitePass= $("#repitePass").val();
    var comentario=$("#comentario").val();

    validaNombre();
    validaApe1();
    validaApe2();
    validaEmail();
    validaPass1();
    validaPass2();
    validaComentario();

    compruebaResultados();

    function validaNombre(){
        if(nombre.length>=25||nombre.length<4||nombre==null){
            alert("La longitud del nombre debe mayor que 4 e inferior a 25 caracteres");
        }else{
            nomb=true;
        }
    }
    function validaApe1(){
        if(primerApe.length>=20||primerApe.length<4||primerApe==null){
            alert("La longitud del primer apellido debe ser mayor que 4 e inferior a 20 caracteres");
        }else{
            ape1=true;
        }
    }
    function validaApe2(){
        if(segunApe.length>=20||segunApe.length<4||segunApe==null){
            alert("La longitud del segundo apellido debe ser mayor que 4 e inferior a 20 caracteres");
        }else{
            ape2=true;
        }
    }
    function validaComentario(){
        if(comentario.length>=120||comentario.length<10||comentario==null){
            alert("La longitud del comentario debe ser mayor que 10 e inferior a 120 caracteres");
        }else{
            coment=true;
        }
    }
    function validaPass1(){
        if(regulaPass(pass)==true){
            contr1=true;
        }else{
            alert("La contraseña no cumple con los parametros especificados");
        }
    }
    function validaPass2(){
        if(repitePass=pass){
            contr2=true;
        }else{
            alert("Las contraseñas no coinciden");
        }
    }
    function validaEmail(){
        if(regulaEmail(email)==true){
            correo=true;
        }else{
            alert("email incorrecto") 
        }
    }
    function regulaPass(pass){
        return expreReguPass.test(pass);
    }
    function regulaEmail(email){
        return expreReguEmail.test(email);
    }
    function compruebaResultados(){
        if(nomb==true&&ape1==true&&ape2==true&&correo==true&&contr1==true&&contr2==true&&coment==true){
            alert("Todo correcto, comentario procesado.");
            window.location.replace("index.php");
        }
    }
}